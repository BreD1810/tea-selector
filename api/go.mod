module github.com/BreD1810/tea-selector/api

go 1.21

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.17
	golang.org/x/crypto v0.12.0
	gopkg.in/yaml.v2 v2.4.0
)
